extends Area2D

export var rotation_speed = 2.6
export var thrust = 500
export var max_vel = 400
export var friction = 0.65

var screen_size = Vector2()
var acceleration = Vector2()
var velocity = Vector2()
var rot = 0

func _ready():
	screen_size = get_viewport_rect().size
	position = screen_size / 2
	set_process(true)
	
func _process(delta):
	if Input.is_action_pressed("player_left"):
		rot -= rotation_speed * delta
	if Input.is_action_pressed("player_right"):
		rot += rotation_speed * delta
	if Input.is_action_pressed("player_thrust"):
		acceleration = Vector2(thrust, 0).rotated(rot)
	else:
		acceleration = Vector2(0, 0)

	acceleration += velocity * -friction
	velocity += acceleration * delta
	
	if position.x >= screen_size.x:
		position.x = 0
	if position.y >= screen_size.y:
		position.y = 0
	if position.x < 0:
		position.x = screen_size.x
	if position.y < 0:
		position.y = screen_size.y
	
	position += velocity * delta
	rotation = rot + PI/2